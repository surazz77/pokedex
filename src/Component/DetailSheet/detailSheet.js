import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import { connect } from "react-redux";
import * as actions from "../../Store/Action/pokemon";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import Buttons from "../Ui/Button/button";
import Profilecard from "../Ui/ProfileCard/profileCard";
import Chart from "../Chart/chart";
import { Bar } from "react-chartjs-2";
import Grid from "@material-ui/core/Grid";
import EvoutionDetail from '../EvolutionDetail/evolutionDetail'

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    marginTop: 80,
    marginLeft: 40,
    marginRight: 40
  },
  mediaCard: {
    maxWidth: 150
  },
  media: {
    height: "10",
    paddingTop: "110%" // 16:9
  },
  card:{
    maxWidth: 1080,
    padding: theme.spacing.unit * 2,
  }
});

const DetailSheet = props => {
  var a = "/pokemon/" + props.id + ".png";
  const { classes } = props;
  useEffect(() => {
    props.getPokemonFullDetail(props.id);
    props.getPokemonSpecies(props.id)
  }, []);
  useEffect(() => {
    return () => {
      props.getPokemonSpecies(0)
      console.log('will unmount');
    }
  }, []);
  if (props.detail == null) {
    return <div>loading...............</div>;
  } else {
    return (
      <div>
        <Paper className={classes.root} elevation={1}>
          <Typography variant="h5" component="h3">
            {props.detail && props.detail.name}
          </Typography>
          <Grid container spacing={24}>
            <Grid item xs={6}>
              <div className={classes.mediaCard}>
                <CardMedia
                  className={classes.media}
                  image={a}
                  title="Paella dish"
                />
              </div>
            </Grid>
            <Grid item xs={6}>
              <Profilecard
                height={props.detail && props.detail["height"]}
                weight={props.detail && props.detail["weight"]}
                ability={
                  props.detail &&
                  props.detail["abilities"].map(a => a.ability.name + ",")
                }
                baseExperience={props.detail && props.detail["base_experience"]}
                baseHappiness={props.speciesInfo && props.speciesInfo['base_happiness']}
                captureRate={props.speciesInfo && props.speciesInfo['capture_rate']}
                eggGroup={props.speciesInfo && props.speciesInfo.egg_groups.map(data=>data.name+',')}
              
              />
            </Grid>
          </Grid>
          {props.detail &&
            props.detail.types.map((name,i) => (
              <Buttons
                name={name.type["name"]}
                key={i}
                // color={
                //   (name.type["name"] == "GRASS" && green) ||
                //   (name.type["name"] == "FIRE" && yellow)
                // }
              />
            ))}

          <Card className={classes.card}>
            <Chart
              type={Bar}
              labels={
                props.detail && props.detail.stats.map(data => data.stat.name)
              }
              data={
                props.detail && props.detail.stats.map(data => data.base_stat)
              }
              height={13}
              width={80}
            />
          </Card>
          <div>
             {/* { props.speciesInfo && <EvoutionDetail speciesInfo={props.speciesInfo}/>} */}
          </div>
        </Paper>
      </div>
    );
  }
};

DetailSheet.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapDispatchToProps = dispatch => {
  return {
    getPokemonFullDetail: id => dispatch(actions.pokemonDetail(id)),
    getPokemonSpecies:id=>dispatch(actions.pokemonSpecies(id))
  };
};

const mapStateToProps = state => {
  return {
    detail: state.detail,
    speciesInfo:state.speciesInfo
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(DetailSheet));
