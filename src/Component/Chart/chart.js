import React, { Component } from "react";
// import { Bar, Line, Radar } from "react-chartjs-2";

class Chart extends Component {
  state = {
    labels: [],
    data: [],
    label: [],
    width: null,
    height: null
  };

  componentDidMount = () => {
    const { labels, width, height } = this.props;
    const { data } = this.props;
    const { label } = this.props;    this.setState({ labels, width, height });
    this.setState({ data });
    this.setState({ label });
  };

  componentDidUpdate=(props)=>{
  }

  render() {
    function getRandomColor() {
      var letters = "0123456789ABCDEF".split("");
      var color = "#";
      for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
    }

    const { labels } = this.state;
    const { data, label, width, height } = this.state;

    const chartdata = {
      width: width,
      labels: labels,
      datasets: [
        {
          label: label,
          data: data,
          backgroundColor: getRandomColor()
        }
      ]
    };

    return (
      <div>
        {(labels && labels.length) > 0 && (
          <this.props.type
            type="horizontalBar"
            data={chartdata}
            width={width}
            height={height}
            options={{
              legend: {
                display: false
              },
              scales: {
                yAxes: [{}],
                xAxes: [
                  {
                    barPercentage: 1.0,
                    categoryPercentage: 0.5,
                    categorySpacing: 1
                  }
                ]
              }
            }}
          />
        )}
      </div>
    );
  }
}
export default Chart;
