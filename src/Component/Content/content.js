import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "../Ui/Card/card";
import Pagination from "../Pagination/pagination";
import { connect } from "react-redux";
import * as actions from "../../Store/Action/pokemon";
import { Grid } from "@material-ui/core";

const styles = theme => ({
  root: {
    flexGrow: 1,
    marginTop: 100
  },
  gridList: {
    width: 500,
    height: 450
  },
  icon: {
    color: "rgba(255, 255, 255, 0.54)"
  }
});

const TitlebarGridList = props => {
  const { classes } = props;
  useEffect(() => {
    props.getPokemonInfo(0);
    props.allPokemon(props.pokemonInfo);
  }, []);

  return (
    <React.Fragment>
      <div className={classes.root}>
        <Grid container spacing={0}>
          {props.pokemonInfo &&
            props.pokemonInfo.map((data, i) => (
              <Card
                name={data.name}
                id={data.url.substr(34, 34).split("/")}
                key={i}
              />
            ))}
        </Grid>
      </div>
      <div>
        <Pagination />
      </div>
    </React.Fragment>
  );
};

TitlebarGridList.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapDispatchToProps = dispatch => {
  return {
    getPokemonInfo: offset => dispatch(actions.pokemons(offset)),
    allPokemon: offset => dispatch(actions.allPokemon(offset))
  };
};

const mapStateToProps = state => {
  return {
    pokemonInfo: state.info,
    allPokemonInfo: state.allPokemonInfo
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(TitlebarGridList));
