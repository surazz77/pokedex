import React from 'react'
import NavBar from '../Navigation/NavBar/navBar'
import DetailSheet from '../DetailSheet/detailSheet'

const PokemonDetail = (props)=>{
    return(
        <React.Fragment>
            <NavBar/>
            <DetailSheet id={props.match.params.number}/>
        </React.Fragment>
    )
}
export default PokemonDetail