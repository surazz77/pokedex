import React from "react";
import NavBar from "../Navigation/NavBar/navBar";
import Content from '../Content/content'
const Main = () => {
  return (
    <React.Fragment>
      <div className="Header">
        <NavBar/>
      </div>
      <div className="content">
        <Content />
      </div>
    </React.Fragment>
  );
};
export default Main;
