import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";


const styles =(theme)=> ({
  card: {
    maxWidth: 250,
    marginTop: 5,
    padding: theme.spacing.unit * 2,
    marginBottom:20

  },
  media: {
    height: 130
  }
})

const MediaCard = (props) => {
  const { classes, name,id } = props;
  var a = '/pokemon/'+id[0]+'.png'
  return (
    <React.Fragment>
      <Grid item xs={3}>
        <Card className={classes.card}>
        <Link to={{ pathname: `/detail/${id[0]}`}} style={{textDecoration:'none',textDecorationColor:'none'}}>
          <CardActionArea>
            <CardMedia
              className={classes.media}
              image={a}
              title="Contemplative Reptile"
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                {name}
              </Typography>
            </CardContent>
          </CardActionArea>
          </Link>
        </Card>
      </Grid>
    </React.Fragment>
  );
}

MediaCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(MediaCard);
