import React, { Fragment } from 'react'
import '../../../App.css'
const Input = (props) => {
    let inputElement = null

    switch (props.elementType) {
        case ('Input'):
            inputElement = <input
                type={props.type}
                value={props.value}
                onChange={props.changed}
                placeholder={props.placeholder}
                checked={props.checked}
                name={props.name}
            />
            break;

        case ('textArea'):
            inputElement = <textarea
                onChange={props.changed}
                rows={props.rows}
                cols={props.cols}
                placeholder={props.placeholder}
                name={props.name}
            />
            break;

        case ('select'):
            inputElement = <select
                name={props.name}>
                {props.options.map(option => (
                    <option key={option.value} value={option.value}>
                        {option}
                    </option>
                ))}
            </select>
            break
        default:
            inputElement = <input
                type={props.type}
                placeholder={props.placeholder}
                value={props.value}
                checked={props.checked}
            />;
    }
    return (
        <Fragment>
            <div className={props.class}>
                <label className='labels'>{props.label}</label><br />
                {inputElement}
            </div>
        </Fragment>
    )
}
export default Input