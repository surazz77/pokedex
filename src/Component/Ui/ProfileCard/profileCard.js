import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";

import Grid from "@material-ui/core/Grid";

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  card: {
    maxWidth: 450,
    marginTop: 5,
    padding: theme.spacing.unit * 2,
    marginBottom: 20
  },
  media: {
    height: 150
  }
});

function ProfileCard(props) {
  const { classes,height,weight,ability,baseExperience,baseHappiness,captureRate,eggGroup } = props;
  return (
    <React.Fragment>
      <Card className={classes.card}>
        <CardActionArea>
          <CardContent>
            <div className={classes.root}>
              <Grid container spacing={24}>
                <Grid item xs={12} sm={6}>
                    Height: {height}m
                </Grid>
                <Grid item xs={12} sm={6}>
                  Weight :{weight}kg
                </Grid>
                <Grid item xs={12} sm={6}>
                    ability: {ability}
                </Grid>
                <Grid item xs={12} sm={6}>
                  Base Experience :{baseExperience}
                </Grid>
                <Grid item xs={12} sm={6}>
                    Base Hapiness: {baseHappiness}
                </Grid>
                <Grid item xs={12} sm={6}>
                  Capture Rate :{captureRate}
                </Grid>
                <Grid item xs={12} sm={6}>
                   Egg Groups:{eggGroup}
                </Grid>
              </Grid>
            </div>
          </CardContent>
        </CardActionArea>
      </Card>
    </React.Fragment>
  );
}

ProfileCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ProfileCard);
