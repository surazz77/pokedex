import React, { useEffect, useState } from "react";
import axios from "axios";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import * as actions from "../../Store/Action/pokemon";
import CardMedia from "@material-ui/core/CardMedia";
const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    marginTop: 80,
    marginLeft: 40,
    marginRight: 40
  },
  mediaCard: {
    maxWidth: 150
  },
  media: {
    height: "10",
    paddingTop: "110%" // 16:9
  },
  card: {
    maxWidth: 1080,
    padding: theme.spacing.unit * 2
  }
});
const EvolutionDetail = props => {
  const { speciesInfo } = props;
  const { classes } = props;
  useEffect(() => {
    props.evolutionInfo(speciesInfo.evolution_chain.url);
  }, []);  
  return (
    <React.Fragment>
      <div style={{ marginTop: "5%" }}>
        Evolution Info
        <Grid container spacing={24}>
          {props.evolutionData &&
            props.evolutionData.map((data, i) => (
              <Grid item xs={4} key={i}>
                <div className={classes.mediaCard}>
                  <CardMedia
                    className={classes.media}
                    image={"/pokemon/" + data + ".png"}
                    title="Paella dish"
                  />
                </div>
              </Grid>
            ))}
        </Grid>
      </div>
    </React.Fragment>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    evolutionInfo: url => dispatch(actions.evolutionInfo(url))
  };
};

const mapStateToProps = state => {
  return {
    evolutionData: state.evolutionData
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(EvolutionDetail));
