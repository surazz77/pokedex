import React, { useState} from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import Pagination from "material-ui-flat-pagination";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import * as actions from "../../Store/Action/pokemon";

const styles = theme => ({
  root: {
      marginTop:50,
      marginLeft:400,
  }
});
const theme = createMuiTheme();

const Page = (props) => {
  const [offset, setOffset] = useState(0);
  const { classes } = props;
  function handleClick(offset) {
    setOffset(offset);
    props.getPokemonInfo(offset)
  }
  return (
    <div className={classes.root}>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <Pagination
          limit={20}
          offset={offset}
          total={800}
          onClick={(e, offset) => handleClick(offset)}
        />
      </MuiThemeProvider>
    </div>
  );
};
const mapDispatchToProps = dispatch => {
  return {
    getPokemonInfo:(offset)=>dispatch(actions.pokemons(offset))
  };
};

export default connect(
  null,mapDispatchToProps
)(withStyles(styles)(Page));

