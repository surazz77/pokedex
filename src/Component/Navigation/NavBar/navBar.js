import React from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import InputBase from "@material-ui/core/InputBase";
import { fade } from "@material-ui/core/styles/colorManipulator";
import { withStyles } from "@material-ui/core/styles";
import SearchIcon from "@material-ui/icons/Search";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import * as actions from "../../../Store/Action/pokemon";

const styles = theme => ({
  root: {
    width: "100%"
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  title: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block"
    },
    textDecoration: "none",
    color: "white"
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    marginRight: theme.spacing.unit * 2,
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing.unit * 3,
      width: "auto"
    }
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  inputRoot: {
    color: "inherit",
    width: "100%"
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: 200
    }
  }
});

class PrimarySearchAppBar extends React.Component {
  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open
    });
  };
  
  searchPokemonName=(e)=>{
    if (e.target.value.length>0){
      this.props.searchPokemon(this.props.allPokemonInfo,e.target.value)
    }
    else{
      this.props.getPokemonInfo(0);
    }
  }

  render() {
    const { classes} = this.props;
    return (
      <div className={classes.root}>
        <AppBar position="fixed">
          <Toolbar>
            {/* <IconButton
              className={classes.menuButton}
              color="inherit"
              aria-label="Open drawer"
              onClick={this.toggleDrawer('left', true)}
            >
              <MenuIcon />
            </IconButton> */}
            <Link to="/" style={{ textDecoration: "none" }}>
              <Typography
                className={classes.title}
                variant="h6"
                color="inherit"
                noWrap
              >
                Pokedex
              </Typography>
            </Link>
            <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon />
              </div>
              <InputBase
                placeholder="Search…"
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput
                }}
                name="pokemon"
                onChange={e => this.searchPokemonName(e)}
              />
            </div>
            <div className={classes.grow} />
          </Toolbar>
        </AppBar>
        <div>
        </div>
      </div>
    );
  }
}

PrimarySearchAppBar.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapDispatchToProps = dispatch => {
  return {
    searchPokemon: (searchList,searchResult) => dispatch(actions.searchPokemon(searchList,searchResult)),
    getPokemonInfo: offset => dispatch(actions.pokemons(offset)),
  };
};

const mapStateToProps = state => {
  return {
    pokemonInfo: state.info,
    allPokemonInfo: state.allPokemonInfo,
    
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(PrimarySearchAppBar));
