import * as actionTypes from "../Action/action.js";
import { updateObject } from "../utility.js";

const intialState = {
  info:null,
  error:null,
  detail:null,
  left:false,
  speciesInfo:null,
  allPokemonInfo:null,
  evolutionData:null
};

const pokemonInfoSuccess = (state = intialState, action) => {
  return updateObject(state, {
    error: null,
    info:action.info
  });
};

const pokemonFail = (state = intialState, action) => {
  return updateObject(state, {
    error: action.error
  });
};

const pokemonFullDetailFail = (state = intialState, action) => {
  return updateObject(state, {
    error: action.error
  });
};

const pokemonFUllDetailSuccess = (state = intialState, action) => {
  return updateObject(state, {
    error: null,
    detail:action.detail
  });
};

const pokemonSpeciesFail = (state = intialState, action) => {
  return updateObject(state, {
    error: action.error
  });
};

const pokemonSpeciesSuccess = (state = intialState, action) => {
  return updateObject(state, {
    error: null,
    speciesInfo:action.speciesInfo
  });
};

const allPokemonFail = (state = intialState, action) => {
  return updateObject(state, {
    error: action.error
  });
};

const allPokemonSuccess = (state = intialState, action) => {
  return updateObject(state, {
    error: null,
    allPokemonInfo:action.allPokemonInfo
  });
};

const pokemonSearchSuccess = (state = intialState, action) => {
  return updateObject(state, {
    error: null,
    info:action.searchPokemonInfo
  });
}

const evolutionInfoFail = (state = intialState, action) => {
  return updateObject(state, {
    error: action.error
  });
};

const evolutionInfoSuccess = (state = intialState, action) => {
  return updateObject(state, {
    error: null,
    evolutionData:action.evolutionData
  });
};


const Reducer = (state = intialState, action) => {
  switch (action.type) {
    case actionTypes.POKEMON_INFO:
      return pokemonInfoSuccess(state, action);
    case actionTypes.POKEMON_FAIL:
      return pokemonFail(state, action);
    case actionTypes.POKEMON_FULL_DETAIL:
      return pokemonFUllDetailSuccess(state, action);
    case actionTypes.POKEMON_FULL_DETAIL_FAIL:
      return pokemonFullDetailFail(state, action);
    case actionTypes.POKEMON_SPECIES_FAIL:
      return pokemonSpeciesFail(state, action);
    case actionTypes.POKEMON_SPECIES:
      return pokemonSpeciesSuccess(state, action);
    case actionTypes.ALL_POKEMON_FAIL:
      return allPokemonFail(state, action);
    case actionTypes.ALL_POKEMON:
      return allPokemonSuccess(state, action);
    case actionTypes.SEARCH_RESULT:
      return pokemonSearchSuccess(state, action);
    case actionTypes.EVOLUTION_INFO:
      return evolutionInfoSuccess(state, action);
    case actionTypes.EVOLUTION_INFO_FAIL:
      return evolutionInfoFail(state, action);
    // case actionTypes.TOOGLE_DRAWER:
    //   return toogleDrawersSuccess(state, action);
    default:
      return state;
  }
};

export default Reducer;
