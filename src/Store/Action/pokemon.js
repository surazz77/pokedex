import * as actionType from "./action";
import axiosInstance from "../../Utils/http";
import _ from "lodash";
import axios from 'axios'

export const pokemonInfo = info => {
  return {
    type: actionType.POKEMON_INFO,
    info: info
  };
};

export const pokemonFail = error => {
  return {
    type: actionType.POKEMON_FAIL,
    error: error
  };
};

export const pokemons = offset => {
  return dispatch => {
    axiosInstance
      .get(`pokemon?offset=${offset}&limit=20`)
      .then(response => {
        dispatch(pokemonInfo(response.data.results));
      })
      .catch(err => {
        dispatch(pokemonFail(err));
      });
  };
};
export const pokemonFullDetail = detail => {
  return {
    type: actionType.POKEMON_FULL_DETAIL,
    detail: detail
  };
};

export const pokemonFullDetailFail = error => {
  return {
    type: actionType.POKEMON_FULL_DETAIL_FAIL,
    error: error
  };
};


export const pokemonDetail = id => {
  return dispatch => {
    axiosInstance
      .get(`pokemon/${id}`)
      .then(response => {
        dispatch(pokemonFullDetail(response.data));
      })
      .catch(err => {
        dispatch(pokemonFullDetailFail(err));
      });
  };
};
 
// export const toogleDrawerSuccess = status => {
//   return {
//     type: actionType.TOOGLE_DRAWER,
//     left: status
//   };
// };

// export const toogleDrawer = status => {
//   return dispatch => {
//     dispatch(toogleDrawerSuccess(status));
//   };
// };

export const pokemonSpeciesFail = error => {
  return {
    type: actionType.POKEMON_FULL_DETAIL_FAIL,
    error: error
  };
};

export const pokemonSpeciesSuccess = speciesInfo => {
  return {
    type: actionType.POKEMON_SPECIES,
    speciesInfo:speciesInfo
  };
};

export const pokemonSpecies = id => {
  return dispatch => {
    axiosInstance.get(`/pokemon-species/${id}`).then(response=>{
      dispatch(pokemonSpeciesSuccess(response.data));
    }).catch(error=>{
      dispatch(pokemonSpeciesFail(error))
    })
  };
};

export const allPokemonFail = error => {
  return {
    type: actionType.ALL_POKEMON_FAIL,
    error: error
  };
};

export const allPokemonSuccess = allPokemonInfo => {
  return {
    type: actionType.ALL_POKEMON,
    allPokemonInfo:allPokemonInfo
  };
};

export const allPokemon = offset=>{
  return dispatch => {
    axiosInstance.get(`pokemon?offset=0&limit=964`).then(response=>{
      dispatch(allPokemonSuccess(response.data.results));
    }).catch(error=>{
      dispatch(allPokemonFail(error))
    })
  };
}

export const searchFail = error => {
  return {
    type: actionType.SEARCH_RESULT_FAIL,
    error: error
  };
};

export const searchResultSuccess = searchPokemonInfo => {
  return {
    type: actionType.SEARCH_RESULT,
    searchPokemonInfo:searchPokemonInfo
  };
};

export const searchPokemon = (searchList,searchElement)=>{
  return dispatch => {
      const results =_.filter(searchList, function(item) {
        var regex = new RegExp('^'+searchElement);
        return item.name.match(regex);
      })
      dispatch(searchResultSuccess(results))

  };
}

export const evolutionInfoFail = error => {
  return {
    type: actionType.EVOLUTION_INFO_FAIL,
    error: error
  };
};

export const evolutionInfoSuccess = evolutionData => {
  return {
    type: actionType.EVOLUTION_INFO,
    evolutionData:evolutionData
  };
};

export const evolutionInfo = (url)=>{
  return dispatch => {
    console.log('dajkdsnldsfm================',url)
    axios
      .get(`${url}`)
      .then(response => {
        var data = [];
        console.log(response.data)
        data.push(response.data.chain.species.url.substr(42).split("/")[0]);
        data.push(
          response.data.chain.evolves_to[0].species.url.substr(42).split("/")[0]
        );
        data.push(
          response.data.chain.evolves_to[0].evolves_to[0].species.url
            .substr(42)
            .split("/")[0]
        );
        console.log('sdaaaaaaaaaaaaaaaaaa',data)
        dispatch(evolutionInfoSuccess(data))

      })
      .catch(error => {
        console.log(error);
      });


  };
}