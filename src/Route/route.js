import React from "react";
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom";
import { connect } from "react-redux";
import Main from '../Component/Main/main'
import PokemonDetail from '../Component/PokemonDetail/pokemonDetail'

const Path = props => {
  return (
    <React.Fragment>
      <Router>
        <div>
          <Route path="/" exact component={Main} />
          <Route path="/detail/:number" exact component={PokemonDetail}/>
        </div>
      </Router>
    </React.Fragment>
  );
};

const mapStateToProps = state => {
  return {
    isAuthenticate: state.token !== null
  };
};

export default connect(
  mapStateToProps,
  null
)(Path);
